package com.example.contactus;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet("/admin/contactus/requests")
public class LoginPage extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        String query = "select username, password from userdata";

        try {
            Class.forName("org.postgresql.Driver");
            Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/loginpage", "postgres", "himanshubhatt");
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);
            HttpSession session = request.getSession();
            PrintWriter out = response.getWriter();

            boolean validUser = false;

            while (rs.next()) {
                String dataUsername = rs.getString(1);
                String dataPassword = rs.getString(2);

                if (username.equals(dataUsername) && password.equals(dataPassword)) {
                    validUser = true;
                    session.setAttribute("yes",true);
                    break;
                }
            }

            if (validUser) {

                RequestDispatcher rd = request.getRequestDispatcher("/requests.jsp");
                rd.forward(request, response);
            } else {

                response.setContentType("text/html");
                out.print("<h3 style='color:red'> Invalid Credentials </h3>");

                RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
                rd.include(request, response);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
