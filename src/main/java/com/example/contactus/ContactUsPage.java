package com.example.contactus;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;


@WebServlet("/submit")
public class ContactUsPage extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        PrintWriter out = response.getWriter();

        String name = request.getParameter("name");
        String mail = request.getParameter("mail");
        String message = request.getParameter("message");

        try{
            Class.forName("org.postgresql.Driver");

            Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/loginpage","postgres","himanshubhatt");

            PreparedStatement pr = con.prepareStatement("insert into responses (name, email, message) values (?,?,?)");

            pr.setString(1,name);
            pr.setString(2,mail);
            pr.setString(3,message);

            int count =pr.executeUpdate();
            if(count>0)
            {
                response.setContentType("text/html");
                out.print("<h3 style='color:green'> Response submitted successfully </h3>");

                RequestDispatcher rd = request.getRequestDispatcher("contactUs.jsp");

                rd.include(request, response);
            }

            else{
                response.setContentType("text/html");
                out.print("<h3 style='color:red'> Failed due to some reason </h3>");

               response.sendRedirect("${pageContext.request.contextPath}/contact-us");
            }

        }
        catch(Exception e){

            e.printStackTrace();
            out.print("<h3 style='color:red'> Error :"+e.getMessage()+" </h3>");

            RequestDispatcher rd = request.getRequestDispatcher("${pageContext.request.contextPath}/admin/login");
        }

    }
}

