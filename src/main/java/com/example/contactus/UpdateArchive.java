package com.example.contactus;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

@WebServlet("/updatearchive")
public class UpdateArchive extends HttpServlet {
    public void service(HttpServletRequest request, HttpServletResponse response) throws IOException {

        try {
            Class.forName("org.postgresql.Driver");
            Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/loginpage", "postgres", "himanshubhatt");

            PreparedStatement pr = con.prepareStatement("update responses set status='Active' where id=?");
            pr.setInt(1, Integer.parseInt(request.getParameter("id")));
            pr.executeUpdate();

            RequestDispatcher rd = request.getRequestDispatcher("/requests.jsp");
            rd.forward(request,response);

        }
        catch(Exception e){
            e.printStackTrace();
        }

    }
}
