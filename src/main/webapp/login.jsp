<%--
  Created by IntelliJ IDEA.
  User: himanshu
  Date: 03/10/23
  Time: 2:38 pm
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Login-Page</title>
  <style>
    body {
      font-family: Arial, sans-serif;
      background-color: #f2f2f2;
      text-align: center;
      margin: 0;
      padding: 0;
    }

    h1 {
      color: #333;
    }

    p {
      color: #666;
    }

    form {
      background-color: #fff;
      width: 300px;
      margin: 0 auto;
      padding: 30px;
      border-radius: 5px;
      box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    }

    label {
      display: block;
      margin-bottom: 10px;
    }

    input[type="text"],
    input[type="password"] {
      width: 100%;
      padding: 10px;
      margin-bottom: 10px;
      border: 1px solid #ccc;
      border-radius: 3px;
    }

    input[type="submit"] {
      background-color: #333;
      color: #fff;
      padding: 10px 20px;
      border: none;
      border-radius: 3px;
      cursor: pointer;
    }

    input[type="submit"]:hover {
      background-color: #555;
    }
  </style>
</head>
<body>

<h1>Admin Login</h1>
<p>Fill your Credentials</p>

<form action="contactus/requests" method="post">

  <label>
    Username: <input type="text" name="username">
  </label>
  <label>
    Password: <input type="password" name="password">
  </label>

  <input type="submit">

</form>

</body>
</html>
