<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<html>
<head>
  <title>Requests Admin Panel</title>
  <link rel="stylesheet" type="text/css" href="styles.css">
  <style>
<%
    if(session.getAttribute("yes")==null){
      RequestDispatcher rd = request.getRequestDispatcher("/login.jsp");
      rd.forward(request,response);
    }
%>
    body {
      font-family: Arial, sans-serif;
      background-color: #f2f2f2;
      text-align: center;
      margin: 0;
      padding: 0;
    }

    h2 {
      color: #333;
      margin-top: 20px;
    }

    table {
      width: 100%;
      border-collapse: collapse;
      margin-top: 20px;
      background-color: #fff;
      box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    }

    th, td {
      padding: 5px; /* Reduced padding here */
      border: 1px solid #ccc;
      text-align: left;
      word-wrap: break-word;
    }

    th.col-id, td.col-id {
      width: 5%;
    }

    th.col-status, td.col-status {
      width: 10%;
    }

    tr:nth-child(even) {
      background-color: #f2f2f2;
    }

    tr:hover {
      background-color: #ddd;
    }

    button {
      background-color: #333;
      color: #fff;
      padding: 5px 10px;
      border: none;
      border-radius: 3px;
      cursor: pointer;
    }

    button:hover {
      background-color: #555;
    }

    .container {
      display: flex;
      justify-content: space-between;
      padding: 20px;
    }
  </style>
</head>

<body>
<h2>Contact Responses</h2>

<div id="active">
  <table>
    <tr>
      <th class="col-id">ID</th>
      <th>User</th>
      <th>Email</th>
      <th>Message</th>
      <th class="col-status">Status</th>
    </tr>
    <%
      String Query = "select * from responses";

      try {
        Class.forName("org.postgresql.Driver");
        Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/loginpage", "postgres", "himanshubhatt");
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(Query);

        while (rs.next()) {
          int id = rs.getInt(1);
          String name = rs.getString(2);
          String email = rs.getString(3);
          String message = rs.getString(4);
          x
          String status = rs.getString(5);
          if(status.equals("Active")) {
    %>
    <tr>
      <td class="col-id"><%= id %></td>
      <td><%= name %></td>
      <td><%= email %></td>
      <td><%= message %></td>
      <td class="col-status"><a href="${pageContext.request.contextPath}/updateactive?id=<%=id%>"><button><%=status%></button></a></td>
    </tr>
    <%
        }
      }
    %>
  </table>
</div>

<div id="archive">
  <br><br><br><br><br><br>
  <table>
    <tr>
      <th class="col-id">ID</th>
      <th>User</th>
      <th>Email</th>
      <th>Message</th>
      <th class="col-status">Status</th>
    </tr>

    <%
      ResultSet rsa = st.executeQuery(Query);

      while (rsa.next()) {
        int id = rsa.getInt(1);
        String name = rsa.getString(2);
        String email = rsa.getString(3);
        String message = rsa.getString(4);
        String status = rsa.getString(5);
        if(status.equals("Archive")) {
    %>
    <tr>
      <td class="col-id"><%= id %></td>
      <td><%= name %></td>
      <td><%= email %></td>
      <td><%= message %></td>
      <td class="col-status"><a href="${pageContext.request.contextPath}/updatearchive?id=<%=id%>"><button><%=status%></button></a></td>
    </tr>
    <%
          }
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    %>
  </table>
</div>
</body>
</html>
